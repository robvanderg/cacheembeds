# Cache Embeddings #

This folder contains the caching implemented using python (gensim).

You have to convert the .txt file to a binary using the c++ code in the convertToBin folder.

All you have to do to cache a full embedding space is:
```
pip3 install --user gensim
python3 cache.py --input text.vec --output text.vec.cache.txt
```

To see all options of this script run without any arguments
```
> python3 cache.py 
please provide imput embeddings (--input)
Usage: cache.py [options]

Contextual Morpheme Tagger

Options:
  -h, --help       show this help message and exit
  --input=INPUT    path to embeddings file
  --output=OUTPUT  path to cached embeddings file
  --beg=BEG        idx of word to start caching
  --end=END        idx of word to end caching
  --showSize       only show size of embeddings
  --topn=TOPN      Number of words to cache
```

When your embeddings are very large, it might be beneficial to split up this process (to run it on multiple machines), for an example on how to do this, see genJobs.py. 

