import gensim
import time
import sys
from optparse import OptionParser
#import os
#os.environ["OMP_NUM_THREADS"] = "1"

if __name__ == '__main__':
    parser = OptionParser(description='Contextual Morpheme Tagger')

    parser.add_option("--input", help="path to embeddings file")
    parser.add_option("--output", help="path to cached embeddings file")
    parser.add_option("--beg", type=int, help="idx of word to start caching")
    parser.add_option("--end", type=int, help="idx of word to end caching")
    parser.add_option("--showSize", action='store_true', default=False, help="only show size of embeddings")
    parser.add_option("--topn", type=int, default=40, help="Number of words to cache")

    (opts, args) = parser.parse_args()
    if opts.input == None:
        print('please provide imput embeddings (--input)')
        parser.print_help()
        exit(1)
    
    if not opts.showSize and opts.output == None:
        print('please specify where to write output (--output)')
        parser.print_help()
        exit(1)

    print("loading " + opts.input + '...')
    model = gensim.models.KeyedVectors.load_word2vec_format(opts.input, unicode_errors='ignore', binary=True)
    print('done!\n')

    if opts.showSize != False:
        print('size of embeds: ' + str(len(model.wv.vocab.keys())))
        exit(0)

    if opts.beg == None:
        opts.beg = 0
    if opts.end == None:
        opts.end = len(model.index_to_key)
    
    print(opts.output)
    outFile = open(opts.output, 'w')
    #have to sort, because order is random otherwise
    words = sorted([word for word in model.index_to_key if type(word) ==str])

    for i in range(opts.beg, opts.end):
        if (i-opts.beg)%1000 == 0:
            print(str(int(i-opts.beg)) + '/' + str(int(opts.end-opts.beg)))
            sys.stdout.flush()

        word = words[i]
        outFile.write(word)

        ##if not all(c.isprintable() for c in word):
        ##    print(word)
        ##    continue

        try:
            for word, dist in model.most_similar(word, topn=opts.topn):
                outFile.write(' ' + word)
                # Yes, I do not save the distance, this saves ram..
        except:
            print('Not found: ' + word)
        
        outFile.write('\n')
    outFile.close()
    

