#include "vocab.ih"

void Vocab::clear()
{
    std::set<std::string> tmp;
    d_collect.swap(tmp);

    d_vocab.clear();
    d_idxs.clear();
    d_idxs.push_back(0);
}
