# Cache Embeddings #
This code contains the necessary classes to load the binary embeddings and query them. Usage in c++ is as follows:

```
Embeds model("text.vecs", "text.vecs.cached.bin");
td::vector<std::string> cands = std::vector<std::string>(40);
std::vector<double> vals = std::vector<double>(40);
model.find(argv[3], &cands[0], &vals[0]);
```

See for an example usage main.cc, which can be used like this:
```
icmbuild
./tmp/bin/binary text.vecs text.vecs.cached.bin hello
```

The original embeddings are still necessary, as distances are not cached. You can use the getDistance function to get the distancein the embeddings space between two strings:
```
Embeds model("text.vecs", "text.vecs.cached.bin");
double dist = model.getDistance("tmrw", "tomorrow");
```

