#include "embeds/embeds.h"
#include "w2v/w2v.h"

#include <string>
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{
    if(argc < 4)
    {
        std::cout << "please provide path to: <embeddings> <embeddingsCache> and give an input word\n";
        exit(1);
    }

    Embeds model(argv[1], argv[2], true);

    std::vector<std::string> cands = std::vector<std::string>(40);
    std::vector<double> vals = std::vector<double>(40);

    if (model.find(argv[3], &cands[0], &vals[0]))
    {
        std::cout << "Top 40 candidates for \"" << argv[3] << "\":\n";
        for (size_t idx = 0; idx != 40; ++idx)
            std::cout << idx << ". " << cands[idx] << '\t' << vals[idx] << '\n';
    }
    else
        std::cout << "Word \"" << argv[3] << "\" not found\n";
}
