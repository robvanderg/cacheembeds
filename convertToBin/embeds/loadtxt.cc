#include "embeds.ih"

void Embeds::loadTxt(string const &path)
{
    d_numCands = 40; //todo make dynamic
    ifstream ifs(path);
    if (!ifs.good())
    {
        cerr << "Could not read: " << path << '\n';
        exit(1);
    }
    
    string line;
    string word;
    string origWord;
    while(getline(ifs, line))
    {
        istringstream iss(line);
        iss >> origWord;
        size_t counter = 0;
        while (iss >> word)
            ++counter;
        if (counter != d_numCands)
            cout << "Only " << counter << " candidates in: \n" << line << '\n';
        d_vocab.addWord(origWord);
    }
    d_vocab.optimize();
    ifs.close();

    d_numCands = 40;
    d_numWords = d_vocab.size();
    d_cands = vector<uint32_t>(d_numWords * d_numCands);
    ifs.open(path);
    while(getline(ifs, line))
    {
        istringstream iss(line);
        iss >> origWord;
        uint32_t origId = d_vocab.getId(origWord);
        size_t counter = 0;
        while (iss >> word)
        {
            uint32_t wordId = d_vocab.getId(word);
            if (wordId == 0)
                cout << word << " not found\n";
            d_cands[origId * d_numCands + counter] = wordId;
            ++counter;
        }
    }
    cout << "Done loading " << path << '\n';
    
}
