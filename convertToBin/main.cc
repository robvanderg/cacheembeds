#include "embeds/embeds.h"
#include "w2v/w2v.h"

#include <string>
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        std::cout << "Please specify <embeddings path> <path of cached words> <output file>\n";
        exit(1);
    }
    std::string origEmbs = argv[1];
    std::string cache = argv[2];
    std::string output = argv[3];

    Embeds init(origEmbs, cache, false);

    std::vector<std::string> cands = std::vector<std::string>(40);
    std::vector<double> vals = std::vector<double>(40);
    //init.find("test", &cands[0], &vals[0]);
    //for (size_t beg = 0; beg != 10; ++beg)
    //    std::cout << beg << ". " << cands[beg] << '\t' << vals[beg] << '\n';

    init.saveBin(output);
    //Embeds init2(origEmbs, output, true);
    //init2.find("test", &cands[0], &vals[0]);
    //for (size_t beg = 0; beg != 10; ++beg)
    //    std::cout << beg << ". " << cands[beg] << '\t' << vals[beg] << '\n';
    
}
