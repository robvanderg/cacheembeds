# Cache Embeddings #

This code is a python wrapper around the c++ code to use the binary cached embeddings code. Unfortunately, this code was written for a slightly older version, and will not work without some small adaptations. If you are interested in this, please contact me: robvanderg@live.nl

Thanks to Ian Matroos for providing this code.

