# Cache Embeddings #

# PLEASE NOTE THAT IS CODE IS VERY SLOW COMPARED TO THE NEW IMPLEMENTATION #
This repository contains an application which can be used to cache the top-n
closest words of all words in the vocabulary.

### Install ###

```
cd src/
icmbuild
```

### Usage ###

To cache embeddings run the binary with six arguments:

* path to word embeddings in word2vec text format
* number of candidates to cache
* number of threads to use
* destination path
* id of word to start with
* id of word to end with

To cache all embeddings of the GoogleNews:
```
./tmp/bin/binary GoogleNews-vectors-negative300.txt 40 8 cached 0 3000000
```

However, this will take very long, so I suggest to use a script like makeJobs.py to run on multiple machines.

### Python Wrapper ###

In the python folder, we included a python wrapper to use cached models. Thanks
to Ian Matroos for providing this code.

### Python caching ###
NEW: now there is a python script to handle the caching, which is based on gensim (and thus faster), see the script in the cachePython folder

