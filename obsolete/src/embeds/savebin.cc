#include "embeds.ih"

void Embeds::saveBin(string const &path)
{
    cerr << "Saving: " << path << '\n';
    ofstream ofs(path);
    if (!ofs.good())
    {
        cerr << "Could not write w2v model: " << path << '\n';
        exit(1);
    }
    d_vocab.saveBin(&ofs);
    ofs.write(reinterpret_cast<char*>(&d_numCands), sizeof(uint64_t));
    ofs.write(reinterpret_cast<char*>(&d_numWords), sizeof(uint64_t));

    ofs.write(reinterpret_cast<char*>(&d_cands[0]), sizeof(uint32_t) * d_numCands * (d_numWords + 1));
    ofs.write(reinterpret_cast<char*>(&d_vals[0]), sizeof(double) * d_numCands * (d_numWords + 1));
    ofs.close();

    
}
