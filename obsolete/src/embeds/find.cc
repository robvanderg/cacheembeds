#include "embeds.ih"

bool Embeds::find(char const *word, string *retCands, double *retVals)
{
    uint32_t wordId = d_vocab.getId(word);
    if(wordId == 0)
        return false;

    cout << wordId <<  '\n';
    if (d_cands[wordId * d_numCands] == 0)
    {
        cerr << "word " << word << " not found!\n";
        vector<string> cands(40);
        vector<float> vals(40);
        bool have = d_rawW2V.find(word, &cands[0], &vals[0]);
        if (!have) //This should never happen?
            return false;
        for (size_t a = 0; a != 40; ++a)
        {
            d_cands[wordId * d_numCands + a] = d_vocab.getId(cands[a]);
            retCands[a] = cands[a];
            d_vals[wordId * d_numCands + a] = double(vals[a]);
            retVals[a] = vals[a];
        }
        d_cachedSomething = true;
    }
    else
    {
        cout << "Found " << word << '\n';
        for (size_t a = 0; a != 40; ++a)
        {
            retCands[a] = d_vocab.getWord(d_cands[wordId * d_numCands + a]);
            retVals[a] = d_vals[wordId * d_numCands + a];
        }
    }
    return true;
}
