#include "embeds.ih"

void Embeds::loadBin(string const &loc)
{
    cerr << "Loading: " << loc << '\n';
    ifstream ifs(loc, ios::binary);
    if (!ifs.good())
    {
        cerr << "No cache found, creating one at: " << loc << '\n';
        d_numCands = 40;
        d_numWords = d_rawW2V.getWords();
        d_cands = vector<uint32_t>(d_numWords * d_numCands);
        d_vals = vector<double>(d_numWords * d_numCands);
        char *vocab = d_rawW2V.getVocab();
        for (int beg = 0; beg != d_numWords; ++beg)
            d_vocab.addWord(string(&vocab[beg * 50]));
        d_vocab.optimize();
        return;
    }
    d_vocab.loadBin(&ifs);

    ifs.read(reinterpret_cast<char*>(&d_numCands), sizeof(uint64_t));
    ifs.read(reinterpret_cast<char*>(&d_numWords), sizeof(uint64_t));
    d_numCands = 40;
    
    d_cands = vector<uint32_t>((d_numWords +1) * d_numCands);
    ifs.read(reinterpret_cast<char*>(&d_cands[0]), sizeof(uint32_t) * (d_numWords + 1) * d_numCands);
    d_vals = vector<double>((d_numWords +1)* d_numCands);
    ifs.read(reinterpret_cast<char*>(&d_vals[0]), sizeof(double) * (d_numWords + 1 )* d_numCands);
    ifs.close();

}

