#include "embeds.ih"

void Embeds::loadTxt(string const &path)
{
    ifstream ifs(path);
    if (!ifs.good())
    {
        cerr << "Could not read w2v model: " << path << '\n';
        exit(1);
    }

    ifs >> d_numCands;
    ifs >> d_numWords; 
    cout << d_numCands << '\t' << d_numWords << '\n';
    d_cands = vector<uint32_t>(d_numCands * d_numWords);
    d_vals = vector<double>(d_numCands * d_numWords);
    
    vector<string> idToWord = vector<string>(d_numWords + 1);
    string word;
    uint32_t id;
    double dist;
    for (int beg = 0; beg != d_numWords ; ++beg)
    {
        ifs >> word;
        idToWord[beg+1] = word;
        d_vocab.addWord(word);
    }
    d_vocab.optimize();
    getline(ifs, word);
    for (int beg = 0; beg != d_numWords; ++beg)
    {
        ifs >> id;
        size_t newId = d_vocab.getId(idToWord[id]);
        for (int candIdx = 0; candIdx != d_numCands; ++candIdx)
        {
            ifs >> id >> dist;
            d_cands[newId * d_numCands + candIdx] = d_vocab.getId(idToWord[id]);
            d_vals[newId * d_numCands + candIdx] = dist;
        }
    }
    cout << "read all\n"; 
}
