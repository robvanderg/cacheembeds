#include "embeds.ih"

void Embeds::saveTxt(string const &path)
{
    cerr << "Saving: " << path << '\n';
    ofstream ofs(path);
    if (!ofs.good())
    {
        cerr << "Could not write w2v model: " << path << '\n';
        exit(1);
    }
    ofs << d_numCands << '\n'
        << d_numWords << '\n';
    d_vocab.save(&ofs);
    
    for (int wordId = 1; wordId != d_numWords + 1; ++wordId)
    {
        ofs << wordId << '\t';
        for (int candIdx = 0; candIdx != d_numCands; ++candIdx)
            ofs << d_cands[wordId * d_numCands + candIdx] << '\t' 
                << d_vals[wordId * d_numCands + candIdx] << '\t';
        ofs << '\n';
    }
    ofs.close();
}

