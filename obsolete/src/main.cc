#include "embeds/embeds.h"
#include "w2v/w2v.h"
#include "convert/convert.h"

#include <string>
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{
    if (argc == 6 or argc == 7)
    {
        int numCands = std::stoi(argv[2]);
        int threads = std::stoi(argv[3]);
        std::string dst(argv[4]);
        if (argc == 7)
        {
            int beg = std::stoi(argv[5]);
            int end = std::stoi(argv[6]);
            Convert myConverter(argv[1], numCands, threads, beg, end);
            myConverter.saveTxt(dst);
        }
        else 
        {
            std::string wordList(argv[5]);
            Convert myConverter(argv[1], numCands, threads, wordList);
            myConverter.saveTxt(dst);
        }
        exit(0);
    }
    else
    {
        std::cout << "Please specify <embeddings path> <number of candidates> <number of threads> <destination path> <word_id begin> <word_id end>\n";
    }
}
