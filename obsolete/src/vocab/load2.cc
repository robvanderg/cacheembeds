#include "vocab.ih"

void Vocab::load(ifstream *ifs)
{
    string word;
    while(getline((*ifs), word))
        addWord(word);
    optimize();
}
