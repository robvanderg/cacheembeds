#include "vocab.ih"

void Vocab::load(string const &path)
{
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read vocab: " << path << '\n';
        exit(1);
    }

    load(&in);
}
