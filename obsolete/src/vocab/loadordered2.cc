#include "vocab.ih"

void Vocab::loadOrdered(ifstream *ifs)
{
    string word;
    while(getline((*ifs), word))
        addOrdered(word);
}
