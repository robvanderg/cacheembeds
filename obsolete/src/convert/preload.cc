#include "convert.ih"

void Convert::preLoad(std::string const &loc)
{
    cerr << "preloading\n";
    ifstream ifs(loc);
    string line;
    while(getline(ifs, line))
    {
        istringstream ss(line);
        string word;
        ss >> word;
        if (word.size() > 49)
            word = word.substr(0,49);
        int wordId = d_wordIds.getId(word);

        if (wordId == 0)
        {
            cout << "error " << word << " not found\n";
            continue;
        }
        vector<uint32_t> cands (40);
        vector<double> vecs (40);
        bool error = false;
        for (size_t beg = 0; beg != 40; ++beg)
        {
            ss >> word;
            int candId = d_wordIds.getId(word);
            if (candId == 0)
            {
                //cout << "error " << word << " not found\n";
                error = true;
                break;
            }
            cands[beg] = candId;
        }
        if (error)
            continue;
        double val;
        for (size_t beg = 0; beg !=40; ++beg)
        {
            ss >> val;
            if (val == 0.0)
            {
                cout << "error, distance of 0.0\n";
                error = true;
                break;
            }
            vecs[beg] = val;
        }
        if (error)
            continue;
        for (size_t beg = 0; beg != 40; ++beg)
        {
            d_cands[wordId * N + beg] = cands[beg];
            d_vals[wordId * N + beg] = vecs[beg];
        }
    }
    cerr << "done\n";
}
