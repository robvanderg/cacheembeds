#include "convert.ih"

void Convert::run(uint16_t numThreads)
{
    cerr << "calculating candidates with " << numThreads << " threads.\n";
    vector<thread> threadVec;
    for (uint16_t threadIdx = 0; threadIdx < numThreads; ++threadIdx)
    {
        thread thr = thread([this] {worker(); });
        threadVec.push_back(move(thr));
    }
    for (uint16_t threadIdx = 0; threadIdx < numThreads; ++threadIdx)
        threadVec.at(threadIdx).join();
}
