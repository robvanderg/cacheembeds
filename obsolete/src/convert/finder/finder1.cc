#include "finder.ih"

Finder::Finder(long long max_size, long long N, long long max_w, long long words, long long size, float *M, char *vocab, uint32_t *cands, double *vals, Vocab *wordIds)
:
   max_size(max_size),
    N(N),
    max_w(max_w),
    words(words),
    size(size), 
    M(M),
    vocab(vocab),
    d_cands(cands), 
    d_vals(vals), 
    d_wordIds(wordIds)
{
    bi = new long long[N];
    bestd = new float[N]; //TODO can be done in *probs?
    bestw = new long long[N];
    vec = new float[max_size];
}
