#include "finder.ih"

void Finder::find(uint32_t wordId)
{
    src = d_wordIds->getWord(wordId);


    for (a = 0; a < N; a++) bestd[a] = 0;
    for (a = 0; a < N; a++) bestw[a] = 0;
    cn = 0;
    b = 0;
    c = 0;
    cn++;
    for (a = 0; a < cn; a++) {  
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], src)) break;
        if (b == words) b = -1;
        bi[a] = b;
        if (b == -1) {
            break;
        }
    }
    for (a = 0; a < size; a++) vec[a] = 0;
    for (b = 0; b < cn; b++) {
        if (bi[b] == -1) continue;
        for (a = 0; a < size; a++) vec[a] += M[a + bi[b] * size];
    }
    len = 0;
    for (a = 0; a < size; a++) len += vec[a] * vec[a];
    len = sqrt(len);
    for (a = 0; a < size; a++) vec[a] /= len;
    for (a = 0; a < N; a++) bestd[a] = -1;
    for (a = 0; a < N; a++) bestw[a] = 0;
    for (c = 0; c < words; c++) {
        a = 0;
        for (b = 0; b < cn; b++) if (bi[b] == c) a = 1;
        if (a == 1) continue;
        dist = 0;
        for (a = 0; a < size; a++) dist += vec[a] * M[a + c * size];
        for (a = 0; a < N; a++) 
        {
            if (dist > bestd[a]) 
            {
                for (d = N - 1; d > a; d--) 
                {
                    bestd[d] = bestd[d - 1];
                    bestw[d] = bestw[d - 1];
                }
                bestd[a] = dist;
                bestw[a] = c;
                break;
            }
        }
    }
    for (a = 0; a < N; a++)
    {
        string cand(&vocab[bestw[a] * max_w]);
        if (cand.size() > 49)
            cand = cand.substr(0,49);
        size_t candId = d_wordIds->getId(cand);
        d_cands[(wordId) * N + a] = candId;
        d_vals[(wordId) * N + a] = bestd[a];
    }
}
