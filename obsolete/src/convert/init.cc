#include "convert.ih"

void Convert::init()
{
    cerr << "initializing\n";

    for (long long wordIdx = 0; wordIdx != words; ++wordIdx)
    {
        string word = string(&vocab[wordIdx * max_w]);
        if (word.size() > 49)
            word = word.substr(0,49);
        d_wordIds.addWord(word);
    }
    d_wordIds.optimize();
    cout << "vocabSize: " << d_wordIds.size() << '\n';

    d_cands = new uint32_t[N * d_wordIds.size()+1];
    d_vals = new double[N * d_wordIds.size()+1];
}
