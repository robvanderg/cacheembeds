#include "convert.ih"

Convert::Convert(string const &location, uint32_t numCands, uint16_t numThreads, string knowns)
:
    N(numCands)
{
    string word;
    ifstream ifs(knowns);
    while (getline(ifs, word))
        d_knowns.addWord(word);
    d_knowns.optimize();
    ifs.close();

    loadW2V(location);
    init();
    getJobs();
    run(numThreads);
}
