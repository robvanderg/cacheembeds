#include "convert.ih"

void Convert::worker()
{
    uint32_t wordId = 0;
    Finder myFinder(max_size, N, max_w, words, size, M, vocab, d_cands, d_vals, &d_wordIds);

    while(true)
    {
        {
            lock_guard<mutex> lk(d_jobMutex);
            if (d_jobs.empty())
                break;
            wordId = d_jobs.front();
            d_jobs.pop();
        }
        myFinder.find(wordId);
        if(d_jobs.size() % 100 == 0)
            cerr << d_jobs.size()  << "/" << d_numJobs << '\t' << getValue1()*1e-6 << '\t' << getValue2()*1e-6 << '\n';
    }
}

