#include "convert.ih"

void Convert::saveBin(string const &loc)
{
    ofstream ofs(loc);
    Vocab writeVocab;
    for (long long wordIdx = 0; wordIdx != words; ++wordIdx)
        writeVocab.addOrdered(&vocab[wordIdx * max_w]);
    writeVocab.saveBin(&ofs);
    
    uint64_t numCands = N;
    ofs.write(reinterpret_cast<char*>(&numCands), sizeof(uint64_t));
    uint64_t vocSize = words;
    ofs.write(reinterpret_cast<char*>(&vocSize), sizeof(uint64_t));

    for (int beg = 0; beg != words * N; ++beg)
        ++d_cands[beg];
    ofs.write(reinterpret_cast<char*>(&d_cands[0]), sizeof(uint32_t) * N * words);
    ofs.write(reinterpret_cast<char*>(&d_vals[0]), sizeof(double) * N * words);
    ofs.close();
}
