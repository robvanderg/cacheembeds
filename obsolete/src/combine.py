import sys
import os

if len(sys.argv) < 2:
    print('please give lang name')

lang = sys.argv[1]
voc = []
data = []

firstFile = open(lang + ".cache100000-200000", errors='ignore', encoding='utf-8')
vecSize = int(firstFile.readline())
size = int(firstFile.readline())
numFiles = (size // 100000) + 1

for i in range(size):
    voc.append(firstFile.readline().strip())
firstFile.close()

for fileIdx in range(numFiles): 
    if (fileIdx != numFiles - 1):
        beg = fileIdx * 100000 if fileIdx != 0 else 1
        end = (fileIdx + 1) * 100000
        inFile = open(lang + '.cache' + str(beg) + '-' + str(end), errors='ignore', encoding='utf-8')
        for i in range(size+2):
            inFile.readline()
        for i in range(100000):
            data.append(inFile.readline().strip())
        inFile.close()
    else:
        lastFile = ''
        for fileName in os.listdir('.'):
            if fileName.startswith(lang+'.cache' + str(fileIdx * 100000) + '-'):
                lastFile = fileName
        inFile = open(lastFile, errors='ignore', encoding='utf-8')
        for i in range(size+2):
            inFile.readline()
        for i in range(fileIdx * 100000, size):
            data.append(inFile.readline().strip())
        inFile.close()


outFile = open(lang + ".cached", 'w')
outFile.write(str(vecSize) + '\n')
outFile.write(str(size) + '\n')
for word in voc:
    outFile.write(word + '\n')
for vec in data:
    outFile.write(vec + '\n')

outFile.close()

