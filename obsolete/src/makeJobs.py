import sys

if len(sys.argv) < 2:
    print("please specify language and size of embeddings")
    exit(1)

lang = sys.argv[2]
size = int(sys.argv[1])

numJobs = (size // 100000) + 1

for jobId in range(numJobs): 
    print(jobId)
    jobFile = open("job"+ "." + lang + "." +  str(jobId), 'w')
    jobFile.write("#!/bin/bash\n\n")
    jobFile.write("#SBATCH --time=40:00:00\n")
    jobFile.write("#SBATCH --nodes=1\n")
    jobFile.write("#SBATCH --ntasks=1\n")
    jobFile.write("#SBATCH --mem=16G\n")
    jobFile.write("#SBATCH --cpus-per-task=12\n")
    jobFile.write("#SBATCH --mail-type=BEGIN,END,FAIL\n")
    jobFile.write("#SBATCH --mail-user=robvanderg@live.nl\n")
    jobFile.write("#SBATCH --job-name=" + lang + "." + str(jobId) + "\n")
    jobFile.write("#SBATCH --output="+ lang + "." + str(jobId) + ".log\n\n")
 
    jobFile.write("module load icmake/7.23.02-foss-2016a\n")

    if (jobId == 0):
        jobFile.write("rm -r tmp\n")
        jobFile.write("icmbuild\n")
        jobFile.write("mkdir /data/p270396/" + lang + ".cache\n")

    jobFile.write("./tmp/bin/binary /data/p270396/" + lang + "  40 12 /data/p270396/" + lang + ".cache ")
    if (jobId != numJobs - 1):
        jobFile.write(str(jobId * 100000) + " " + str((jobId + 1) * 100000) + '\n')
    else:
        jobFile.write(str(jobId * 100000) + " " + str(size) + '\n')
    jobFile.close()



