# Cache Embeddings #

This repository contains code which can be used to cache the top-n closest words of all words in the vocabulary of word embeddings.

### Contents ###
This repository contains multiple code-bases, which are all in their own folder. Inside each folder you can find a detailed readme:

* cacheEmbeds: code to cache embeddings, based on gensim. Writes to a text file, which can be converted to binary for faster usage
* convertToBin: code to convert the cache saved in text format to a binary format, which can be used in python and c++
* obsolete: the old c++ version to cache embeddings (based on the original word2vec code), is much slower compared to the gensim implementation.
* useCPP: the source code to use the binary format in c++, see main.cc for an example.
* usePython: python handle to use the binary format. Is not adapted to the new c++ version yet (todo), if you are interested in this, please contact me: robvanderg@live.nl. Thanks to Ian Matroos for providing this code!

### Typical usage ###
Below I show the commands of a typical use-case scenario. This assumes there is a file text.txt, and trains embeddings, caches them, and queries them using the c++ code.

```
./word2vec -train ~/text.txt -output ~/text.vecs -cbow 0 -binary 1
cd cacheembeds/cacheEmbeds
python3 cache.py --input ~/text.vecs --output ~/text.vecs.cached.txt
cd ../convertToBin
icmbuild
./tmp/bin/binary ~/text.vecs ~/text.vecs.cached.txt ~/text.vecs.cached.bin
cd ../useCPP
.icmbuild
./tmp/bin/binary ~/text.vecs ~/text.vecs.cached.bin hello
```

### Pre-cached embeddings ###
I used this code to speed up my lexical normalization model [MoNoise](https://bitbucket.org/robvanderg/monoise/). For this use, I cached embeddings trained on tweets for several languages. You can find these embeddings (+cache) here: http://robvandergoot.com/data/monoise/


### Citation ###
Please use the following citation if you use this tool:
```
@inproceedings{vandergoot-monoise,
    title = "MoNoise: A Multi-lingual and Easy-to-use Lexical Normalization Tool",
    author = "van der Goot, Rob",
    booktitle = "Proceedings of {ACL} 2019, System Demonstrations",
    month = July,
    year = "2019",
    address = "Florence, Italy",
    publisher = "Association for Computational Linguistics",
}
```

### FAQ ###
Nobody has asked me any questions about this yet, but feel free to be the first: robvanderg@live.nl

